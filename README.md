### Mes outils pour utiliser R.

------------------------------------------------------------------------

#### Pour créer le package:

-   `library(usethis)`

-   `create_package("outils")`


#### Ajouter des fonctions dans le dossier R puis les documenter avec rOxygen

-   `#' Print Hello, World #'`

-   `#' This function prints "Hello, World!" to the console.`

-   `#' @export`

-   `hello_world <- function() { print("Hello, World!") }`

`devtools::document()` permet d'éditer les fichiers .Rd de documentation dans le dossier man et le fichier NAMESPACE


#### Pour builder le package (cela crée un .tar.gz):

`devtools::build()`


#### Pour tester en local:

-   `install.packages("path/to/your/package.tar.gz", repos = NULL, type = "source")`

-   `library(votre_nom_de_package)hello_world()`



#### Arborescence minimale d'un package R:

mon_package/\
├── DESCRIPTION\
├── NAMESPACE\
├── R/ │\
├── fichier_fonction1.R │\
└── fichier_fonction2.R

├── man/ │\
├── fonction1.Rd\
└── fonction2.Rd

└── README.md


#### Pour installer le package depuis gitlab.com:

-   `library(remotes)`

-   `remotes::install_gitlab("douxvalkyn/outils")`


### Pour mettre à jour le package:

- `unloadNamespace("outils")`

- `remove.packages("outils")`

- `remotes::install_git("https://gitlab.com/douxvalkyn/outils")`